package server;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.BackEndInstance;
import server.ServerDatabaseCommunicator;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private BackEndInstance backEnd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        backEnd = new BackEndInstance();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	    	
		
	        response.setContentType("text/html;charset=UTF-8");

	        String email = request.getParameter("email");
	        String pass = request.getParameter("pass");
	        
	        	
	    		Long passNum = (long) 0;
	    		int passcheck = 0;
	    		if(pass != null){
	    		for (int i = 0; i < pass.length(); i++) {
	    			char temp = pass.charAt(i);
	    			int num = (int) temp;
	    			Long power = (long) Math.pow((double)128, (double)(pass.length() - 1 - i));
	    			passNum = passNum + (power * num);
	    		
	    		}
	    		
	    		System.out.println("passNum" + passNum);
	    		passcheck = passNum.intValue();
	    		System.out.println("passcheck:" + passcheck);
	    		
	    		}
	      
	        //convert the password here into encrypted form
	 
	    		
	        
	        if(backEnd.GetSDC().checkUser(email, passNum))
	        {
	        	System.out.println("it checked");
	        	request.setAttribute("BackEnd", backEnd);
	        	request.getSession().setAttribute("BackEnd", backEnd);
	        	this.getServletConfig().getServletContext().setAttribute("BackEnd", backEnd);
	        	
	        	RequestDispatcher rs = request.getRequestDispatcher("main");
	            rs.forward(request, response);
	        }
	        else
	        {
	        	
	        		System.out.println("Username or Password incorrect");
	        		RequestDispatcher rs = request.getRequestDispatcher("login3.html");
	        		rs.include(request, response);
	        	
	        //if (email != null && email != "") {
	        	//request.getRequestDispatcher("main").forward(request, response);
	        	
	        //}
	       // }

	}

}
}
