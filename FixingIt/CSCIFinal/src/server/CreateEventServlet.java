package server;

import java.io.IOException;
import java.sql.Date;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datastructures.Event;
import datastructures.Preference;
import datastructures.User;

@WebServlet("/CreateEventServlet")
public class CreateEventServlet extends HttpServlet {
	
	private BackEndInstance backEnd;
	public CreateEventServlet() {
		super();
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		backEnd = (BackEndInstance) request.getAttribute("BackEnd");
	    response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");
        String shortDes = request.getParameter("short");
        String longDes = request.getParameter("long");
        String time = request.getParameter("time");
        String date = request.getParameter("date");
        String hour = time.substring(0, 1);
        String min = time.substring(1, time.length()-1);
        String month = date.substring(0, 1);
        String day = date.substring(3, 4);
        String year = date.substring(6, date.length()-1);
        Date d = new Date(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
        Vector<Preference> preVector = new Vector<Preference>();
        User currUser = backEnd.GetMainUser();
	       
        preVector.addElement(Preference.Business);
        //Create a new event with this information and add it to the database/vector
        Event newEvent = new Event(1, name, d, currUser, longDes, preVector);
        ServerDatabaseCommunicator sdc = backEnd.GetSDC();
        backEnd.AddEvent(newEvent);
        sdc.addEvent(newEvent);
	        
	        RequestDispatcher rs = request.getRequestDispatcher("create_event.html");
	        rs.include(request, response);
	       // }

	}
}
