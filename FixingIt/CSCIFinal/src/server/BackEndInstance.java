package server;

import java.io.Serializable;
import java.util.Vector;

import datastructures.Event;
import datastructures.User;

public class BackEndInstance implements Serializable {

	private static final long serialVersionUID = 1L;
	Vector<User> AllUsers;
	Vector<Event> AllEvents;
	ServerDatabaseCommunicator mySDC;
	User MainUser;
	
	public BackEndInstance(){
		mySDC = new ServerDatabaseCommunicator(this);
		MainUser = null;
		
		AllUsers = mySDC.ParseAllUsers();
		AllEvents = mySDC.ParseAllEvents();
		mySDC.AddUserConnections();
		
	}
	
	public void UpdateInstance(){
		
	}
	
	public void SetUserInstance(User mainUser){
		MainUser = mainUser;
	}
	
	public ServerDatabaseCommunicator GetSDC(){
		return mySDC;
	}
	
	public Vector<User> GetAllUsers(){
		return AllUsers;
	}
	
	public Vector<Event> GetAllEvents(){
		return AllEvents;
	}
	
	public User GetMainUser() {
		return MainUser;
	}
	
	public void AddEvent(Event newevent){
		AllEvents.add(newevent);
		
	}
	
	public void Register(User newUser, long password){
		MainUser = newUser;
		mySDC.addUser(newUser, password);
	}
	
	/*public static void main(String [] args){
	BackEndInstance bei = new BackEndInstance();
	String name = "csnyder";
	int password = 1234567;
	if(bei.GetSDC().checkUser(name, password)){
		System.out.println("The database works. That's cool");
	}
}*/
	
}
