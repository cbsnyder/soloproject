package datastructures;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;
import java.util.Vector;

public class Event implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8332887873968377983L;
	
	private String EventName;
	private Integer EventID;
	private Date EventDate;
	private User HostingUser;
	private Vector<User> Attendees;
	private String EventDescription;
	private Vector<Preference> EventTypes;
	private Vector<Comment> Comments;
	private boolean IsPast;
	private boolean IsCanceled;
	
	public Event(String name, Date local_time) {
		
		EventName = name;
		
		EventDate = local_time;
		
	}
	
	public Event(Integer id, String name, Date date, User host, String descript, Vector<Preference> types){
		EventID = id;
		EventName = name;
		EventDate = date;
		HostingUser = host;
		Attendees = new Vector<User>();
		EventDescription = descript;
		EventTypes = types;
		Comments = new Vector<Comment>();
		IsPast = false;
		IsCanceled = false;
	}
	
	public void AddAttendee(User newattend){
		Attendees.add(newattend);
	}
	
	
	
	public void RemoveAttendee(User exattend){
		Attendees.remove(exattend);
	}
	
	public String GetName(){
		return EventName;
	}
	
	public Integer GetEventID(){
		return EventID;
	}
	
	public Date GetDate(){
		return EventDate;
	}
	
	public User GetHost(){
		return HostingUser;
	}
	
	public Vector<Preference> GetEventTypes(){
		return EventTypes;
	}
	
	public String GetDescription(){
		return EventDescription;
	}
	
	public Vector<Comment> GetComments(){
		return Comments;
	}
	
	public void AddComment(Comment newcomment){
		Comments.add(newcomment);
	}
	
	public Vector<User> GetAttendees(){
		return Attendees;
	}
	
	public Integer GetNumberAttending(){
		return Attendees.size();
	}
	
	public boolean EventIsPast(){
		return IsPast;
	}
	
	public boolean EventIsCanceled(){
		return IsCanceled;
	}
	
	public void Cancel(){
		IsCanceled = true;
	}
}
