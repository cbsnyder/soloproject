package datastructures;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class User implements Serializable{
	
	private static final long serialVersionUID = 8116618159901797312L;
	
	//Private User Variables
	private String Username;

	private String Email;

	private Vector<Event> AttendingEvents;
	private Vector<Event> PastEvents;
	private Vector<Event> UpcomingEvents;
	private Vector<Event> HostingEvents;
	private Vector<User> UserConnections;
	private Vector<Preference> EventPreferences;
	private Date Birthday;
	
	public volatile ArrayList<Event> TopEvents;
	
	//Public User Methods 
	public User(String username, String email){
		
		Username = username;
		//Encrypted = password;
		Email = email;
		//EventPreferences = pref;
		AttendingEvents = new Vector<Event>();
		HostingEvents = new Vector<Event>();
		UserConnections = new Vector<User>();
		//Birthday = birth;
		Date today = Calendar.getInstance().getTime();
		Event e1 = new Event("Thanksgiving", today);
		
		TopEvents = new ArrayList<Event>();
		
		TopEvents.add(e1);
		
		Event e2 = new Event("Christmas Party", today);
		
		TopEvents.add(e2);
		
		Event e3 = new Event("NYE Party", today);
		
		TopEvents.add(e3);
		//Encryption algorithm for password to get Integer equivalent
	}
	
	public void HostEvent(Event e){
		HostingEvents.add(e);
	}
	
	public void AddToEvent(Event newevent){
		AttendingEvents.add(newevent);
		UpcomingEvents.add(newevent);
	}
	
	public void RSVPEvent(Event newevent){
		newevent.AddAttendee(this);
		AttendingEvents.add(newevent);
		UpcomingEvents.add(newevent);
	}
	
	public void unRSVPEvent(Event exevent){
		exevent.RemoveAttendee(this);
		AttendingEvents.remove(exevent);
		UpcomingEvents.remove(exevent);
	}
	
	public void CancelEvent(Event event){
		event.Cancel();
	}
	
	public void AddConnection(User newfriend){
		UserConnections.add(newfriend);
	}
	
	public void AddPreference(Preference pref){
		EventPreferences.add(pref);
	}
	
	public void RemovePreference(Preference pref){
		EventPreferences.remove(pref);
	}
	
	
	public void ResetPassword(String newpass){
		//Go through encryption algorithm and set the encrypted password to the derived Integer
	}
	
	public Vector<Event> GetPastEvents(){
		return PastEvents;
	}
	
	public Vector<Event> GetUpcomingEvents(){
		return UpcomingEvents;
	}
	
	public Vector<Event> GetAllEvents(){
		return AttendingEvents;
	}
	
	public Vector<Event> GetHostingEvents(){
		return HostingEvents;
	}
	
	public Vector<User> GetConnections(){
		return UserConnections;
	}
	
	public Vector<Preference> GetEventPreferences(){
		return EventPreferences;
	}
	
	public Date GetBirthday(){
		return Birthday;
	}
	
	public String GetEmail(){
		return Email;
	}
	
	public String GetUserName(){
		return Username;
	}

}
