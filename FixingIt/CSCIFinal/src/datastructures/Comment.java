package datastructures;

import java.time.LocalTime;
import java.util.ArrayList;

public class Comment {
	private User Poster;
	private LocalTime DatePosted;
	private String CommentBody;
	private boolean IsReply;
	private boolean HasReplies;
	private Comment ParentComment;
	private ArrayList<Comment> ChildComments;
	
	public Comment(User poster, LocalTime date, String body, boolean isreply, Comment parent){
		Poster = poster;
		//this can be replaced with a way to get the current time instead of passing 
		//it into the constructor(DatePosted):
		DatePosted = date;
		CommentBody = body;
		IsReply = isreply;
		if(IsReply){
			ParentComment = parent;
			ParentComment.AddReply(this);
		} else{
			ParentComment = null;
		}
		
		HasReplies = false;
		ChildComments = new ArrayList<Comment>();
	}
	
	public void AddReply(Comment newreply){
		HasReplies = true;
		ChildComments.add(newreply);
	}
	
	public User GetPoster(){
		return Poster;
	}
	
	public LocalTime GetDate(){
		return DatePosted;
	}
	
	public String GetBodyText(){
		return CommentBody;
	}
	
	public Comment GetParent(){
		return ParentComment;
	}
	
	public ArrayList<Comment> GetChildren(){
		return ChildComments;
	}
}
