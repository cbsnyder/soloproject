package datastructures;

//enums are automatically serializable
public enum Preference {
		Business{
			@Override
			public String toString(){
				return "Business";
			}
		}, Social{
			@Override
			public String toString(){
				return "Social";
			}
		}, Food{
			@Override
			public String toString(){
				return "Food";
			}
		}, Greek{
			@Override
			public String toString(){
				return "Greek";
			}
		}, Networking{
			@Override
			public String toString(){
				return "Networking";
			}
		}, Religious{
			@Override
			public String toString(){
				return "Religious";
			}
		}, Charity{
			@Override
			public String toString(){
				return "Charity";
			}
		}, Party{
			@Override
			public String toString(){
				return "Party";
			}
		}
		

}
